package br.com.itau.cacaniquel;

import java.util.ArrayList;
import java.util.List;

public class Maquina {
	private List<Slot> slots;
	private int pontuacaoTotal;
	
	public Maquina(int quantidadeSlots) {
		slots = new ArrayList<>();
		
		for(int i = 0; i < quantidadeSlots; i++) {
			slots.add(new Slot());
		}
	}
	
	public void sortear() {
		for(Slot slot: slots) {
			slot.sortear();
			pontuacaoTotal += slot.getUltimoSorteio().getValor();
		}
	}
	
	public int getPontuacaoTotal() {
		return pontuacaoTotal;
	}
	
	public List<Slot> getSlots() {
		return slots;
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("| ");
		
		for(Slot slot: slots) {
			builder.append(slot);
			builder.append(" | ");
		}
		
		builder.append("\nPontuação total: ");
		builder.append(pontuacaoTotal);
		
		return builder.toString();
		
	}
}
