package br.com.itau.cacaniquel;

public class App {
	public static void main(String[] args) {
		Maquina maquina = new Maquina(3);
		
		maquina.sortear();
		
		Impressora.imprimir(maquina);
	}
}
