package br.com.itau.cacaniquel;

public enum Simbolo {
	BANANA("Banana", 10),
	FRAMBOESA("Framboesa", 30),
	MOEDA("Moeda", 100),
	SETE("Sete", 300);
	
	private String nome;
	private int valor;
	
	private Simbolo(String nome, int valor) {
		this.nome = nome;
		this.valor = valor;
	}
	
	public static int tamanho() {
		return values().length;
	}
	
	public String getNome() {
		return nome;
	}
	
	public int getValor() {
		return valor;
	}
}
